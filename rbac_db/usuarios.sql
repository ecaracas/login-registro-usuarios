-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2018 at 03:30 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usuarios`
--

-- --------------------------------------------------------

--
-- Table structure for table `rbac_menus`
--

CREATE TABLE `rbac_menus` (
  `id_rbac_menu` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `url_tipo` decimal(5,0) NOT NULL,
  `categoria` varchar(20) NOT NULL,
  `estatus` decimal(1,0) DEFAULT '1',
  `orden` decimal(5,0) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `modulo` varchar(100) DEFAULT NULL,
  `jerarquia` float DEFAULT '0',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_editado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `creado_por` decimal(5,0) DEFAULT NULL,
  `editado_por` decimal(5,0) DEFAULT NULL,
  `eliminado_por` decimal(5,0) DEFAULT NULL,
  `nivel` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rbac_menu_opciones`
--

CREATE TABLE `rbac_menu_opciones` (
  `id_rbac_menu_opcion` int(11) UNSIGNED NOT NULL,
  `id_rbac_menu` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `opcion` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `url_tipo` decimal(5,0) DEFAULT NULL,
  `categoria` varchar(20) NOT NULL,
  `estatus` decimal(1,0) DEFAULT '1',
  `orden` decimal(5,0) NOT NULL,
  `icono` varchar(50) DEFAULT NULL,
  `jerarquia` float DEFAULT '0',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_editado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `creado_por` decimal(5,0) NOT NULL,
  `editado_por` decimal(5,0) DEFAULT NULL,
  `eliminado_por` decimal(5,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rbac_roles`
--

CREATE TABLE `rbac_roles` (
  `id_rbac_rol` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `estatus` decimal(1,0) NOT NULL DEFAULT '1',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_editado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `creado_por` decimal(5,0) DEFAULT NULL,
  `editado_por` decimal(5,0) DEFAULT NULL,
  `eliminado_por` decimal(5,0) DEFAULT NULL,
  `principal` float DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rbac_roles_opciones`
--

CREATE TABLE `rbac_roles_opciones` (
  `id_rbac_rol_opcion` int(11) UNSIGNED NOT NULL,
  `id_rbac_rol` int(11) UNSIGNED NOT NULL,
  `estatus` decimal(1,0) DEFAULT '1',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_editado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `creado_por` decimal(5,0) NOT NULL,
  `editado_por` decimal(5,0) DEFAULT NULL,
  `eliminado_por` decimal(5,0) DEFAULT NULL,
  `id_rbac_menu_opcion` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rbac_roles_opciones_usuarios`
--

CREATE TABLE `rbac_roles_opciones_usuarios` (
  `id_rbac_rol_opcion_usuario` int(11) UNSIGNED NOT NULL,
  `id_rbac_usuario` int(11) UNSIGNED NOT NULL,
  `id_rbac_rol_opcion` int(11) UNSIGNED NOT NULL,
  `estatus` decimal(1,0) DEFAULT '1',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_editado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `creado_por` decimal(5,0) DEFAULT NULL,
  `editado_por` decimal(5,0) DEFAULT NULL,
  `eliminado_por` decimal(5,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rbac_usuarios`
--

CREATE TABLE `rbac_usuarios` (
  `id_rbac_usuario` int(11) UNSIGNED NOT NULL,
  `id_rbac_rol` int(11) UNSIGNED NOT NULL,
  `nombreusuario` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `estatus` decimal(1,0) DEFAULT '1',
  `dni_numero` varchar(20) NOT NULL,
  `telf_local_numero` varchar(20) NOT NULL,
  `cambio_clave` decimal(5,0) DEFAULT '0',
  `lastlogin_time` timestamp NULL DEFAULT NULL,
  `dni_letra` tinytext,
  `fecha_creado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_editado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `creado_por` decimal(5,0) NOT NULL,
  `editado_por` decimal(5,0) DEFAULT NULL,
  `eliminado_por` decimal(5,0) DEFAULT NULL,
  `contrasena` varchar(256) DEFAULT NULL,
  `access_token` varchar(256) DEFAULT NULL,
  `authkey` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rbac_menus`
--
ALTER TABLE `rbac_menus`
  ADD PRIMARY KEY (`id_rbac_menu`),
  ADD UNIQUE KEY `id_rbac_menu` (`id_rbac_menu`);

--
-- Indexes for table `rbac_menu_opciones`
--
ALTER TABLE `rbac_menu_opciones`
  ADD PRIMARY KEY (`id_rbac_menu_opcion`),
  ADD UNIQUE KEY `id_rbac_menu_opcion` (`id_rbac_menu_opcion`),
  ADD KEY `id_rbac_menu` (`id_rbac_menu`);

--
-- Indexes for table `rbac_roles`
--
ALTER TABLE `rbac_roles`
  ADD PRIMARY KEY (`id_rbac_rol`),
  ADD UNIQUE KEY `id_rbac_rol` (`id_rbac_rol`);

--
-- Indexes for table `rbac_roles_opciones`
--
ALTER TABLE `rbac_roles_opciones`
  ADD PRIMARY KEY (`id_rbac_rol_opcion`),
  ADD UNIQUE KEY `id_rbac_rol_opcion` (`id_rbac_rol_opcion`),
  ADD KEY `id_rbac_rol` (`id_rbac_rol`),
  ADD KEY `id_rbac_menu_opcion` (`id_rbac_menu_opcion`);

--
-- Indexes for table `rbac_roles_opciones_usuarios`
--
ALTER TABLE `rbac_roles_opciones_usuarios`
  ADD PRIMARY KEY (`id_rbac_rol_opcion_usuario`),
  ADD UNIQUE KEY `id_rbac_rol_opcion_usuario` (`id_rbac_rol_opcion_usuario`),
  ADD KEY `id_rbac_usuario` (`id_rbac_usuario`),
  ADD KEY `id_rbac_rol_opcion` (`id_rbac_rol_opcion`);

--
-- Indexes for table `rbac_usuarios`
--
ALTER TABLE `rbac_usuarios`
  ADD PRIMARY KEY (`id_rbac_usuario`),
  ADD UNIQUE KEY `id_rbac_usuario` (`id_rbac_usuario`),
  ADD KEY `id_rbac_rol` (`id_rbac_rol`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rbac_menus`
--
ALTER TABLE `rbac_menus`
  MODIFY `id_rbac_menu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rbac_menu_opciones`
--
ALTER TABLE `rbac_menu_opciones`
  MODIFY `id_rbac_menu_opcion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rbac_roles`
--
ALTER TABLE `rbac_roles`
  MODIFY `id_rbac_rol` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rbac_roles_opciones`
--
ALTER TABLE `rbac_roles_opciones`
  MODIFY `id_rbac_rol_opcion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rbac_roles_opciones_usuarios`
--
ALTER TABLE `rbac_roles_opciones_usuarios`
  MODIFY `id_rbac_rol_opcion_usuario` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rbac_usuarios`
--
ALTER TABLE `rbac_usuarios`
  MODIFY `id_rbac_usuario` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rbac_menu_opciones`
--
ALTER TABLE `rbac_menu_opciones`
  ADD CONSTRAINT `rbac_menu_opciones_ibfk_1` FOREIGN KEY (`id_rbac_menu`) REFERENCES `rbac_menus` (`id_rbac_menu`);

--
-- Constraints for table `rbac_roles_opciones`
--
ALTER TABLE `rbac_roles_opciones`
  ADD CONSTRAINT `rbac_roles_opciones_ibfk_1` FOREIGN KEY (`id_rbac_rol`) REFERENCES `rbac_roles` (`id_rbac_rol`),
  ADD CONSTRAINT `rbac_roles_opciones_ibfk_2` FOREIGN KEY (`id_rbac_menu_opcion`) REFERENCES `rbac_menu_opciones` (`id_rbac_menu_opcion`);

--
-- Constraints for table `rbac_roles_opciones_usuarios`
--
ALTER TABLE `rbac_roles_opciones_usuarios`
  ADD CONSTRAINT `rbac_roles_opciones_usuarios_ibfk_1` FOREIGN KEY (`id_rbac_rol_opcion`) REFERENCES `rbac_roles_opciones` (`id_rbac_rol_opcion`),
  ADD CONSTRAINT `rbac_roles_opciones_usuarios_ibfk_2` FOREIGN KEY (`id_rbac_usuario`) REFERENCES `rbac_usuarios` (`id_rbac_usuario`);

--
-- Constraints for table `rbac_usuarios`
--
ALTER TABLE `rbac_usuarios`
  ADD CONSTRAINT `rbac_usuarios_ibfk_1` FOREIGN KEY (`id_rbac_rol`) REFERENCES `rbac_roles` (`id_rbac_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
