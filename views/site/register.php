<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="register-form">
    <h3><?= $msg ?></h3>

    <div class="login-logo">
        <a href="#"><b>Registrar</b>Usuario</a>
    </div>


    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'id' => 'formulario',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]);
    ?>
    
    <?= $form->field($model, "username")->input("text") ?>   
    

    
    <?= $form->field($model, "email")->input("email") ?>   
    

    
    <?= $form->field($model, "password")->input("password") ?>   
    

    
    <?= $form->field($model, "password_repeat")->input("password") ?>   
    

    
        <div class="form-group">
            <?= Html::submitButton("guardar", ["class" => "btn btn-primary"]) ?>
        </div>
    


<?php $form->end() ?>
</div>