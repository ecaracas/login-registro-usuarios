<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$this->title = 'Acceso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <?php 
        Modal::begin([
            // 'header'=>'register',
            'id'=>'modal',
            'size'=>'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";

        Modal::end();
    ?>
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>Login</b>Usuarios</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Ingresar Usuario</p>

                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                    ],
                ]); ?>

                    <?= $form->field(
                        $model,
                        'username',
                        [
                            'options' =>
                                [
                                'tag' => 'div',
                                'class' => 'form-group field-loginform-username has-feedback required'
                            ],
                            'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
                                             {error}{hint}'
                        ]
                    )->textInput(['placeholder' => 'usuario'])
                    ?>

                    <?= $form->field(
                        $model,
                        'password',
                        [
                            'options' =>
                                [
                                'tag' => 'div',
                                'class' => 'form-group field-loginform-password has-feedback required'
                            ],
                            'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                               {error}{hint}'
                        ]
                    )->passwordInput(['placeholder' => 'contraseña']) ?>




                    <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-11">
                            <?= Html::submitButton('ingresar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            <?= Html::button('registrarse', ['value'=>Url::to('index.php?r=site/register'),'class'=>'btn btn-primary','id'=>'modalButton']) ?>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
